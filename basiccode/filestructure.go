package basiccode

/*
Printf has over a dozen such conversions, which Go programmers cal l verb s. This table is far
from a complete specification but illustrates many of the features that are avai lable:

%d de imal integer
%x, %o, %b integer in hexade cimal, octal, binar y
%f, %g, %e floating-p oint number: 3.141593 3.141592653589793 3.141593e+00
%t boole an: true or false
%c rune (Unico de co de point)
%s st ring
%q quot ed str ing "abc" or rune 'c'
%v any value in a natural for mat
%T type of any value
%% literal percent sig n (no operand)
*/

import (
	"fmt"
)

const boilingF = 212.0

func FileStructure() {
	var f = boilingF
	var c = (f - 32) * 5 / 9
	fmt.Printf("boiling point = %g°F or %g°C\n", f, c)
	// Output:
	// boiling point = 212°F or 100°C
}
