package main

import (
	"fmt"
	"image/png"
	"learn-go/basiccode"
	"os"
)

func main() {
	// basiccode.FileStructure()
	// basiccode.Echo2()

	img := basiccode.MandelbrotPainter()
	fmt.Println("code generated")

	f, _ := os.Create("output.png")

	png.Encode(f, img)

	fmt.Println("file created")

}
