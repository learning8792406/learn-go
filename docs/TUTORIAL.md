# Tutorial
&larr; [Back to Content](./CONTENT.md)
## Basic 

Hello world program in go
```go
// rohitpjpti18/learn-go/basiccode/tutorial.go
package basiccode

import "fmt"

func HelloWorld() {
	fmt.Println("Hello, World")
}
```

The Go toolchain converts a source program and the things it dep ends on into instructions in the native machine language of a computer. These tools are accessed through a single command called go that has a number of sub commands. The simplest of these sub command s is run, which compiles the source code from one or more source files whose names end in .go, links it with libraries, then runs the resulting executable file.

```bash
$ go run helloworld.go
```

If the program is more than a one-shot experiment, it’s likely that you would want to compile it once and save the compiled result for later use. That is done with go build:

```bash
$ go build helloworld.go
```

This creates an executable binary file called helloworld that can be run any time without further processing:

```bash
$ ./helloworld
Hello, World
```

## Command Line Arguments

Here’s an imp lementation of the Unix echo command, which prints its command-line arguments
on a single line.
```go
// Echo1 prints its command-line arguments.
// rohitpjpti18/learn-go/basiccode/tutorial.go
package basiccode

import (
    "fmt"
    "os"
)

func Echo1() {
	var s, sep string
	for i := 1; i < len(os.Args); i++ {
		s += sep + os.Args[i]
		sep = " "
	}
	fmt.Println(s)
}
```

Printf has over a dozen such conversions, which Go programmers cal l verb s. This table is far
from a complete specification but illustrates many of the features that are avai lable:

| format Specifier	| Description |
| ----------------- | ----------- |
| %d 				| decimal integer
| %x, %o, %b 		| integer in hexadecimal, octal, binary
| %f, %g, %e 		| floating-point number: 3.141593 3.141592653589793 3.141593e+00
| %t 				| boolean: true or false
| %c 				| rune (Unicode code point)
| %s 				| string
| %q 				| quoted string "abc" or rune 'c'
| %v				| any value in a natural format
| %T 				| type of any value
| %% 				| literal percent sig n (no operand)