## Content

&larr; [Back to README](../README.md)

- [x] [Introduction](./TUTORIAL.md)
- [x] [Program Structure](./PROGRAMSTRUCTURE.md)
- [x] [Basic Data Types](./BASICDATATYPES.md)
