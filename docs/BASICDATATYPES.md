# Basic Data Types
&larr; [Back to Content](./CONTENT.md)

In addition, there are about three dozen predeclared names like `int` and `true` for built-in constants, types, and functions:
|       |           | 
|-------| --------- | 
| Constants: | `true false iota nil` | 
| Types:  | `int int8 int16 int32 int64 uint uint8 uint16 uint32 uint64 uintptr float32 float64 complex128 complex64 bool byte rune string error`     | 
| Functions:  | `make len cap new append copy close delete complex real imag panic recover`| 

## Integers

Go provides both signed and unsigned integer arithmetic. There are four distinct sizes of
signed integers — 8, 16, 32, and 64 bits — represented by the types `int8`, `int16`, `int32`, and `int64`, and corresponding unsigned versions `uint8`, `uint16`, `uint32`, and `uint64`

Go’s binar y op erator s for arithmetic, logic, and comparison are liste d here in order of decre asing
pre cedence:
```go
*   /   %   <<  >>  &   &^
+   -   |   ^
==  !=  <   <=  >   >=
&&
||
```

|       |                           | 
|-------| ------------------------- | 
| ==    | equal to                  | 
| !=    | not equal to              | 
| <     | less than                 | 
| <=    | less than or equal to     |
| >     | greater than              |   
| >=    | greater than or equal to  |
| &     | bitwise AND               |
| \|    | bitwise OR                |
| ^     | bitwise XOR               |
| &^    | bit clear (AND NOT)       |
| <<    | left shift                |
| >>    | right shift               |

## String
 

|       |                           | 
|-------| ------------------------- | 
| \a    | "alert" or bell           | 
| \b    | backspace             | 
| \f    | form feed                 | 
| \n    | newline     |
| \r    | carriage return              |   
| \t    | tab  |
| \v    | vertical tab              |
| \'    |  single quote (only in the rune lit eral '\'')               |
| \"    | double quote (only wit hin "..." literals)               |
| \\    | backslash                |


Unicode escapes in Go string literals allow us to specify them by their numeric code point value. There are two forms, \uhhhh for a 16-bit
value and \Uhhhhhhhh for a 32-bit value, where each h is a hexadecimal digit; the need for the 32-bit form arises very infrequently. Each denotes the UTF-8 encoding of the specified code poin. Thus, for example, the following string literals all represent the same six-byte string:

```
"\xe4\xb8\x96\xe7\x95\x8c"
"\u4e16\u754c"
"\U00004e16\U0000754c"
```


## Constants
Constants are expressions whose value is known to the compiler and whose evaluation is guaranteed to occur at compile time, not at run time. The underly ing typ e of every constant is a basic type: boolean, string, or number.

